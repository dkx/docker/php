#!/usr/bin/env sh

/configure

if [[ "${WAIT_FOR_FILES_TO_EXISTS}" == "1" ]]; then
	while :
	do
		echo "Waiting for files to exists"
		if [[ ! -z "$(ls -A /app)" ]]; then
			break
		fi
		sleep 2
	done
fi

docker-php-entrypoint "$@"
