#!/usr/bin/env sh

if [[ -f /php_configured ]]; then
	exit
fi

touch /php_configured

if [[ ! -d /tmp/opcache ]]; then
	mkdir /tmp/opcache
fi

COMMON_HOST=host.docker.internal
MAC_HOST=docker.for.mac.host.internal

ping -q -c1 ${COMMON_HOST} > /dev/null 2>&1
if [[ $? -eq 0 ]]; then
	echo "xdebug.remote_host=${COMMON_HOST}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
else
	ping -q -c1 ${MAC_HOST} > /dev/null 2>&1
	if [[ $? -eq 0 ]]; then
		echo "xdebug.remote_host=${MAC_HOST}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	else
		HOST_IP=$(ip route | awk 'NR==1 {print $3}')
		echo "xdebug.remote_host=${HOST_IP}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	fi
fi

PHP_OPCACHE_MAX_ACCELERATED_FILES=$(expr $(find . -type f -print | grep php | wc -l) + 100)

echo "opcache.max_accelerated_files=${PHP_OPCACHE_MAX_ACCELERATED_FILES}" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

if [[ -n "${PHP_XDEBUG_IDEKEY}" ]]; then
	echo "xdebug.idekey=${PHP_XDEBUG_IDEKEY}" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

if [[ "${PHP_XDEBUG}" == "0" ]]; then
	sed -i "s/zend_extension=/; zend_extension=/" /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
	sed -i "s/xdebug\./; xdebug./"  /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

if [[ "${PHP_XDEBUG_LOG}" == "1" ]]; then
	echo "xdebug.remote_log=/var/log/xdebug.log" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

if [[ -n "${PHP_OPCACHE}" ]]; then
	sed -i "s/opcache.enable=0/opcache.enable=${PHP_OPCACHE}/" /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
	sed -i "s/opcache.enable_cli=0/opcache.enable_cli=${PHP_OPCACHE}/" /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
fi

if [[ -n "${PHP_UPLOAD_LIMIT}" ]]; then
	echo "upload_max_filesize = ${PHP_UPLOAD_LIMIT}" >> /usr/local/etc/php/php.ini
	echo "post_max_size = ${PHP_UPLOAD_LIMIT}" >> /usr/local/etc/php/php.ini
fi

if [[ -n "${PHP_TIMEZONE}" ]]; then
	echo "[PHP]" > /usr/local/etc/php/conf.d/tzone.ini
	echo 'date.timezone = "'${PHP_TIMEZONE}'"' >> /usr/local/etc/php/conf.d/tzone.ini
fi
